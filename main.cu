#include <cassert>
#include <chrono>
#include <functional>
#include <iomanip>
#include <iostream>
#include <stdexcept>
#include <thread>
#include <tuple>
#include <utility>
#include <vector>

#include <cuda.h>
#include <cuda_runtime.h>

#define CHECK_CUDA_ERROR(val) check((val), #val, __FILE__, __LINE__)
template <typename T>
void check(T err, const char *const func, const char *const file,
           const int line) {
  if (err != CUDA_SUCCESS) {
    std::cerr << "CUDA Runtime Error at: " << file << ":" << line << std::endl;
    const char *string = nullptr;
    cuGetErrorString(err, &string);
    std::cerr << string << " " << func << std::endl;
    std::exit(EXIT_FAILURE);
  }
}

#define CHECK_LAST_CUDA_ERROR() checkLast(__FILE__, __LINE__)
void checkLast(const char *const file, const int line) {
  cudaError_t err{cudaGetLastError()};
  if (err != cudaSuccess) {
    std::cerr << "CUDA Runtime Error at: " << file << ":" << line << std::endl;
    std::cerr << cudaGetErrorString(err) << std::endl;
    std::exit(EXIT_FAILURE);
  }
}

#define THREADS_PER_BLOCk 1024
#define BLOCKS_PER_GRID 32

__global__ void float_addition(float *output, float const *input_1,
                               float const *input_2, uint32_t n) {
  const uint32_t idx{blockDim.x * blockIdx.x + threadIdx.x};
  const uint32_t stride{blockDim.x * gridDim.x};
  for (uint32_t i{idx}; i < n; i += stride) {
    output[i] = input_1[i] + input_2[i];
  }
}

void initialize_host_memory(float *h_buffer, uint32_t n, float value) {
  for (unsigned int i{0}; i < n; ++i) {
    h_buffer[i] = value;
  }
}

bool verify_host_memory(float *h_buffer, uint32_t n, float value) {
  for (unsigned int i{0}; i < n; ++i) {
    if (h_buffer[i] != value) {
      return false;
    }
  }
  return true;
}

void run_benchmark(const char *name, float *h_input_1, float *h_input_2,
                   float *h_output, float v_input_1, float v_input_2,
                   float v_output, float v_output_reference, uint32_t n,
                   std::function<void(void)> function_input,
                   std::function<void(void)> function_kernel,
                   std::function<void(void)> function_output, CUstream stream,
                   int num_repeats = 100, int num_warmups = 10) {
  const int n_events = 10;
  std::vector<CUevent> events(n_events);
  for (int i = 0; i < n_events; i++) {
    CHECK_CUDA_ERROR(cuEventCreate(&events[i], CU_EVENT_DEFAULT));
  }

  std::vector<float> runtimes(n_events / 2);

  for (int i = 0; i < num_repeats + num_warmups; i++) {
    // Initialize
    CHECK_CUDA_ERROR(cuEventRecord(events[0], stream));
    initialize_host_memory(h_input_1, n, v_input_1);
    initialize_host_memory(h_input_2, n, v_input_2);
    initialize_host_memory(h_output, n, v_output);
    CHECK_CUDA_ERROR(cuEventRecord(events[1], stream));

    // Input
    CHECK_CUDA_ERROR(cuEventRecord(events[2], stream));
    function_input();
    CHECK_CUDA_ERROR(cuEventRecord(events[3], stream));

    // Kernel execution
    CHECK_CUDA_ERROR(cuEventRecord(events[4], stream));
    function_kernel();
    CHECK_CUDA_ERROR(cuEventRecord(events[5], stream));

    // Output
    CHECK_CUDA_ERROR(cuEventRecord(events[6], stream));
    function_output();
    CHECK_CUDA_ERROR(cuEventRecord(events[7], stream));

    // Validation
    CHECK_CUDA_ERROR(cuEventRecord(events[8], stream));
    assert(verify_host_memory(h_output, n, v_output_reference));
    CHECK_CUDA_ERROR(cuEventRecord(events[9], stream));

    if (i >= num_warmups) {
      CHECK_CUDA_ERROR(cuStreamSynchronize(stream));
      for (int j = 0; j < n_events; j += 2) {
        CUevent start = events[j];
        CUevent stop = events[j + 1];
        float time;
        CHECK_CUDA_ERROR(cuEventElapsedTime(&time, start, stop));
        runtimes[j / 2] += time / num_repeats;
      }
    }
  }

  std::cout << std::fixed << std::setprecision(3);
  std::cout << ">> " << name << std::endl;

  // Individual runtimes
  std::vector<std::string> labels{"init", "input", "kernel", "output",
                                  "verify"};
  assert(labels.size() == runtimes.size());
  for (int i = 0; i < runtimes.size(); i++) {
    std::cout << labels[i] << ": " << runtimes[i] << " ms" << std::endl;
  }
  const size_t sizeof_input = 2 * n * sizeof(float);
  const size_t sizeof_output = 1 * n * sizeof(float);

  // Combined runtime
  const float runtime_total = runtimes[1] + runtimes[2] + runtimes[3];
  std::cout << "total: " << runtime_total << " ms (input+kernel+output)"
            << std::endl;

  // Bandiwdth
  const float bw_input = sizeof_input / runtimes[1];
  const float bw_kernel = (sizeof_input + sizeof_output) / runtimes[2];
  const float bw_output = sizeof_output / runtimes[3];
  const float bw_total = (sizeof_input + sizeof_output) / runtime_total;
  std::cout << "input bandwidth : ";
  if (bw_input < 10 * bw_kernel) {
    std::cout << bw_input * 1e-6 << " GB/s" << std::endl;
  } else {
    std::cout << " n/a" << std::endl;
  }
  std::cout << "kernel bandwidth: " << bw_kernel * 1e-6 << " GB/s" << std::endl;
  std::cout << "output bandwidth: ";
  if (bw_output < 10 * bw_kernel) {
    std::cout << bw_output * 1e-6 << " GB/s" << std::endl;
  } else {
    std::cout << " n/a" << std::endl;
  }
  std::cout << "total bandwidth : " << bw_total * 1e-6 << " GB/s" << std::endl;

  for (int i = 0; i < n_events; i++) {
    CHECK_CUDA_ERROR(cuEventDestroy(events[i]));
  }
}

void report_device_memory(CUdevice device) {
  size_t free;
  size_t total;
  CHECK_CUDA_ERROR(cuMemGetInfo(&free, &total));
  std::cout << "memory: " << float(free) / 1e9 << "/" << float(total) / 1e9
            << " GB (free/total)" << std::endl;
}

void benchmark_allochost_alloc(const char *name, int n, float v_input_1,
                               float v_input_2, float v_output,
                               float v_output_reference, CUstream stream,
                               CUdevice device, int num_repeats,
                               int num_warmups) {
  float *h_input_1, *h_input_2, *h_output;
  float *d_input_1, *d_input_2, *d_output;

  // cuMemAllocHost
  CHECK_CUDA_ERROR(
      cuMemAllocHost(reinterpret_cast<void **>(&h_input_1), n * sizeof(float)));
  CHECK_CUDA_ERROR(
      cuMemAllocHost(reinterpret_cast<void **>(&h_input_2), n * sizeof(float)));
  CHECK_CUDA_ERROR(
      cuMemAllocHost(reinterpret_cast<void **>(&h_output), n * sizeof(float)));

  // cuMemAlloc
  CHECK_CUDA_ERROR(cuMemAlloc(
      reinterpret_cast<CUdeviceptr *>(reinterpret_cast<void **>(&d_input_1)),
      n * sizeof(float)));
  CHECK_CUDA_ERROR(cuMemAlloc(
      reinterpret_cast<CUdeviceptr *>(reinterpret_cast<void **>(&d_input_2)),
      n * sizeof(float)));
  CHECK_CUDA_ERROR(cuMemAlloc(
      reinterpret_cast<CUdeviceptr *>(reinterpret_cast<void **>(&d_output)),
      n * sizeof(float)));

  std::function<void()> function_input = [&]() {
    CHECK_CUDA_ERROR(cuMemcpyHtoDAsync(
        reinterpret_cast<CUdeviceptr>(reinterpret_cast<void *>(d_input_1)),
        h_input_1, n * sizeof(float), stream));
    CHECK_CUDA_ERROR(cuMemcpyHtoDAsync(
        reinterpret_cast<CUdeviceptr>(reinterpret_cast<void *>(d_input_2)),
        h_input_2, n * sizeof(float), stream));
  };
  std::function<void()> function_kernel = [&]() {
    dim3 const threads_per_block{THREADS_PER_BLOCk};
    dim3 const blocks_per_grid{BLOCKS_PER_GRID};
    float_addition<<<blocks_per_grid, threads_per_block, 0, stream>>>(
        d_output, d_input_1, d_input_2, n);
    CHECK_LAST_CUDA_ERROR();
  };
  std::function<void()> function_output = [&]() {
    CHECK_CUDA_ERROR(cuMemcpyDtoHAsync(h_output,
                                       reinterpret_cast<CUdeviceptr>(d_output),
                                       n * sizeof(float), stream));
    CHECK_CUDA_ERROR(cuStreamSynchronize(stream));
  };

  run_benchmark(name, h_input_1, h_input_2, h_output, v_input_1, v_input_2,
                v_output, v_output_reference, n, function_input,
                function_kernel, function_output, stream, num_repeats,
                num_warmups);

  report_device_memory(device);
  std::cout << std::endl;

  CHECK_CUDA_ERROR(cuMemFree(reinterpret_cast<CUdeviceptr>(d_input_1)));
  CHECK_CUDA_ERROR(cuMemFree(reinterpret_cast<CUdeviceptr>(d_input_2)));
  CHECK_CUDA_ERROR(cuMemFree(reinterpret_cast<CUdeviceptr>(d_output)));
  CHECK_CUDA_ERROR(cuMemFreeHost(h_input_1));
  CHECK_CUDA_ERROR(cuMemFreeHost(h_input_2));
  CHECK_CUDA_ERROR(cuMemFreeHost(h_output));
}

void benchmark_allochost(const char *name, int n, float v_input_1,
                         float v_input_2, float v_output,
                         float v_output_reference, CUstream stream,
                         CUdevice device, int num_repeats, int num_warmups) {
  float *h_input_1, *h_input_2, *h_output;
  float *m_input_1, *m_input_2, *m_output;

  // cuMemAllocHost
  CHECK_CUDA_ERROR(
      cuMemAllocHost(reinterpret_cast<void **>(&h_input_1), n * sizeof(float)));
  CHECK_CUDA_ERROR(
      cuMemAllocHost(reinterpret_cast<void **>(&h_input_2), n * sizeof(float)));
  CHECK_CUDA_ERROR(
      cuMemAllocHost(reinterpret_cast<void **>(&h_output), n * sizeof(float)));

  // cuMemHostGetDevicePointer
  CHECK_CUDA_ERROR(cuMemHostGetDevicePointer(
      reinterpret_cast<CUdeviceptr *>(reinterpret_cast<void **>(&m_input_1)),
      h_input_1, 0));
  CHECK_CUDA_ERROR(cuMemHostGetDevicePointer(
      reinterpret_cast<CUdeviceptr *>(reinterpret_cast<void **>(&m_input_2)),
      h_input_2, 0));
  CHECK_CUDA_ERROR(cuMemHostGetDevicePointer(
      reinterpret_cast<CUdeviceptr *>(reinterpret_cast<void **>(&m_output)),
      h_output, 0));

  std::function<void()> function_input = [&]() {};
  std::function<void()> function_kernel = [&]() {
    dim3 const threads_per_block{THREADS_PER_BLOCk};
    dim3 const blocks_per_grid{BLOCKS_PER_GRID};
    float_addition<<<blocks_per_grid, threads_per_block, 0, stream>>>(
        m_output, m_input_1, m_input_2, n);
    CHECK_LAST_CUDA_ERROR();
  };
  std::function<void()> function_output = [&]() {
    CHECK_CUDA_ERROR(cuStreamSynchronize(stream));
  };

  run_benchmark(name, h_input_1, h_input_2, h_output, v_input_1, v_input_2,
                v_output, v_output_reference, n, function_input,
                function_kernel, function_output, stream, num_repeats,
                num_warmups);

  report_device_memory(device);
  std::cout << std::endl;

  CHECK_CUDA_ERROR(cuMemFreeHost(h_input_1));
  CHECK_CUDA_ERROR(cuMemFreeHost(h_input_2));
  CHECK_CUDA_ERROR(cuMemFreeHost(h_output));
}

void benchmark_hostalloc(const char *name, int n, float v_input_1,
                         float v_input_2, float v_output,
                         float v_output_reference, CUstream stream,
                         CUdevice device, unsigned int flags, int num_repeats,
                         int num_warmups) {
  float *h_input_1, *h_input_2, *h_output;

  // cuMemAllocHost (with optional flags for input)
  CHECK_CUDA_ERROR(cuMemHostAlloc(reinterpret_cast<void **>(&h_input_1),
                                  n * sizeof(float), flags));
  CHECK_CUDA_ERROR(cuMemHostAlloc(reinterpret_cast<void **>(&h_input_2),
                                  n * sizeof(float), flags));
  CHECK_CUDA_ERROR(cuMemHostAlloc(reinterpret_cast<void **>(&h_output),
                                  n * sizeof(float), 0));

  std::function<void()> function_input = [&]() {};
  std::function<void()> function_kernel = [&]() {
    dim3 const threads_per_block{THREADS_PER_BLOCk};
    dim3 const blocks_per_grid{BLOCKS_PER_GRID};
    float_addition<<<blocks_per_grid, threads_per_block, 0, stream>>>(
        h_output, h_input_1, h_input_2, n);
    CHECK_LAST_CUDA_ERROR();
  };
  std::function<void()> function_output = [&]() {
    CHECK_CUDA_ERROR(cuStreamSynchronize(stream));
  };

  run_benchmark(name, h_input_1, h_input_2, h_output, v_input_1, v_input_2,
                v_output, v_output_reference, n, function_input,
                function_kernel, function_output, stream, num_repeats,
                num_warmups);

  report_device_memory(device);
  std::cout << std::endl;

  CHECK_CUDA_ERROR(cuMemFreeHost(h_input_1));
  CHECK_CUDA_ERROR(cuMemFreeHost(h_input_2));
  CHECK_CUDA_ERROR(cuMemFreeHost(h_output));
}

void benchmark_allochost_hostpointer(const char *name, int n, float v_input_1,
                                     float v_input_2, float v_output,
                                     float v_output_reference, CUstream stream,
                                     CUdevice device, int num_repeats,
                                     int num_warmups) {
  float *h_input_1, *h_input_2, *h_output;

  // cuMemAllocHost
  CHECK_CUDA_ERROR(
      cuMemAllocHost(reinterpret_cast<void **>(&h_input_1), n * sizeof(float)));
  CHECK_CUDA_ERROR(
      cuMemAllocHost(reinterpret_cast<void **>(&h_input_2), n * sizeof(float)));
  CHECK_CUDA_ERROR(
      cuMemAllocHost(reinterpret_cast<void **>(&h_output), n * sizeof(float)));

  std::function<void()> function_input = [&]() {};
  std::function<void()> function_kernel = [&]() {
    dim3 const threads_per_block{THREADS_PER_BLOCk};
    dim3 const blocks_per_grid{BLOCKS_PER_GRID};
    float_addition<<<blocks_per_grid, threads_per_block, 0, stream>>>(
        h_output, h_input_1, h_input_2, n);
    CHECK_LAST_CUDA_ERROR();
  };
  std::function<void()> function_output = [&]() {
    CHECK_CUDA_ERROR(cuStreamSynchronize(stream));
  };

  run_benchmark(name, h_input_1, h_input_2, h_output, v_input_1, v_input_2,
                v_output, v_output_reference, n, function_input,
                function_kernel, function_output, stream, num_repeats,
                num_warmups);

  report_device_memory(device);
  std::cout << std::endl;

  CHECK_CUDA_ERROR(cuMemFreeHost(h_input_1));
  CHECK_CUDA_ERROR(cuMemFreeHost(h_input_2));
  CHECK_CUDA_ERROR(cuMemFreeHost(h_output));
}

void benchmark_allocmanaged(const char *name, int n, float v_input_1,
                            float v_input_2, float v_output,
                            float v_output_reference, CUstream stream,
                            CUdevice device, int num_repeats, int num_warmups,
                            int flags, bool prefetch) {
  float *d_input_1, *d_input_2, *d_output;

  // cuMemAllocManaged
  CHECK_CUDA_ERROR(cuMemAllocManaged(
      reinterpret_cast<CUdeviceptr *>(reinterpret_cast<void **>(&d_input_1)),
      n * sizeof(float), flags));
  CHECK_CUDA_ERROR(cuMemAllocManaged(
      reinterpret_cast<CUdeviceptr *>(reinterpret_cast<void **>(&d_input_2)),
      n * sizeof(float), flags));
  CHECK_CUDA_ERROR(cuMemAllocManaged(
      reinterpret_cast<CUdeviceptr *>(reinterpret_cast<void **>(&d_output)),
      n * sizeof(float), flags));

  std::function<void()> function_input = [&]() {
    if (prefetch) {
      CHECK_CUDA_ERROR(cuMemPrefetchAsync(
          reinterpret_cast<CUdeviceptr>(reinterpret_cast<void *>(d_input_1)),
          n * sizeof(float), device, stream));
      CHECK_CUDA_ERROR(cuMemPrefetchAsync(
          reinterpret_cast<CUdeviceptr>(reinterpret_cast<void *>(d_input_2)),
          n * sizeof(float), device, stream));
    }
  };
  std::function<void()> function_kernel = [&]() {
    dim3 const threads_per_block{THREADS_PER_BLOCk};
    dim3 const blocks_per_grid{BLOCKS_PER_GRID};
    float_addition<<<blocks_per_grid, threads_per_block, 0, stream>>>(
        d_output, d_input_1, d_input_2, n);
    CHECK_LAST_CUDA_ERROR();
  };
  std::function<void()> function_output = [&]() {
    if (prefetch) {
      CHECK_CUDA_ERROR(cuMemPrefetchAsync(
          reinterpret_cast<CUdeviceptr>(reinterpret_cast<void *>(d_output)),
          n * sizeof(float), CU_DEVICE_CPU, stream));
    }
    CHECK_CUDA_ERROR(cuStreamSynchronize(stream));
  };

  run_benchmark(name, d_input_1, d_input_2, d_output, v_input_1, v_input_2,
                v_output, v_output_reference, n, function_input,
                function_kernel, function_output, stream, num_repeats,
                num_warmups);

  report_device_memory(device);
  std::cout << std::endl;

  CHECK_CUDA_ERROR(cuMemFree(reinterpret_cast<CUdeviceptr>(d_input_1)));
  CHECK_CUDA_ERROR(cuMemFree(reinterpret_cast<CUdeviceptr>(d_input_2)));
  CHECK_CUDA_ERROR(cuMemFree(reinterpret_cast<CUdeviceptr>(d_output)));
}

void benchmark_hybrid(const char *name, int n, float v_input_1, float v_input_2,
                      float v_output, float v_output_reference, CUstream stream,
                      CUdevice device, int num_repeats, int num_warmups) {
  float *d_input_1, *d_input_2, *d_output;
  float *h_output;

  CHECK_CUDA_ERROR(cuMemAllocManaged(
      reinterpret_cast<CUdeviceptr *>(reinterpret_cast<void **>(&d_input_1)),
      n * sizeof(float), CU_MEM_ATTACH_GLOBAL));
  CHECK_CUDA_ERROR(cuMemAllocManaged(
      reinterpret_cast<CUdeviceptr *>(reinterpret_cast<void **>(&d_input_2)),
      n * sizeof(float), CU_MEM_ATTACH_GLOBAL));
  CHECK_CUDA_ERROR(cuMemAlloc(
      reinterpret_cast<CUdeviceptr *>(reinterpret_cast<void **>(&d_output)),
      n * sizeof(float)));
  CHECK_CUDA_ERROR(cuMemHostAlloc(reinterpret_cast<void **>(&h_output),
                                  n * sizeof(float), 0));

  std::function<void()> function_input = [&]() {
    CHECK_CUDA_ERROR(cuMemPrefetchAsync(
        reinterpret_cast<CUdeviceptr>(reinterpret_cast<void *>(d_input_1)),
        n * sizeof(float), device, stream));
    CHECK_CUDA_ERROR(cuMemPrefetchAsync(
        reinterpret_cast<CUdeviceptr>(reinterpret_cast<void *>(d_input_2)),
        n * sizeof(float), device, stream));
  };
  std::function<void()> function_kernel = [&]() {
    dim3 const threads_per_block{THREADS_PER_BLOCk};
    dim3 const blocks_per_grid{BLOCKS_PER_GRID};
    float_addition<<<blocks_per_grid, threads_per_block, 0, stream>>>(
        d_output, d_input_1, d_input_2, n);
    CHECK_LAST_CUDA_ERROR();
  };
  std::function<void()> function_output = [&]() {
    CHECK_CUDA_ERROR(cuMemcpyDtoHAsync(h_output,
                                       reinterpret_cast<CUdeviceptr>(d_output),
                                       n * sizeof(float), stream));
    CHECK_CUDA_ERROR(cuStreamSynchronize(stream));
  };

  run_benchmark(name, d_input_1, d_input_2, h_output, v_input_1, v_input_2,
                v_output, v_output_reference, n, function_input,
                function_kernel, function_output, stream, num_repeats,
                num_warmups);

  report_device_memory(device);
  std::cout << std::endl;

  CHECK_CUDA_ERROR(cuMemFree(reinterpret_cast<CUdeviceptr>(d_input_1)));
  CHECK_CUDA_ERROR(cuMemFree(reinterpret_cast<CUdeviceptr>(d_input_2)));
  CHECK_CUDA_ERROR(cuMemFree(reinterpret_cast<CUdeviceptr>(d_output)));
  CHECK_CUDA_ERROR(cuMemFreeHost(h_output));
}

int main() {
  constexpr int const num_repeats{3};
  constexpr int const num_warmups{1};

  constexpr int const n{32000000};

  CHECK_CUDA_ERROR(cuInit(0));

  CUdevice device;
  CHECK_CUDA_ERROR(cuDeviceGet(&device, 0));

  CUcontext context;
  CHECK_CUDA_ERROR(cuCtxCreate(&context, 0, device));

  CUstream stream;
  CHECK_CUDA_ERROR(cuStreamCreate(&stream, CU_STREAM_DEFAULT));

  float const v_input_1{1.0f};
  float const v_input_2{1.0f};
  float const v_output{0.0f};
  float const v_output_reference{v_input_1 + v_input_2};

  benchmark_allochost_alloc("cuMemAllocHost+cuMemAlloc", n, v_input_1,
                            v_input_2, v_output, v_output_reference, stream,
                            device, num_repeats, num_warmups);
  benchmark_allochost("cuMemAllocHost+cuMemHostGetDevicePointer", n, v_input_1,
                      v_input_2, v_output, v_output_reference, stream, device,
                      num_repeats, num_warmups);

  int property;
  unsigned int flags = 0;

  CHECK_CUDA_ERROR(cuDeviceGetAttribute(
      &property, CU_DEVICE_ATTRIBUTE_CAN_USE_HOST_POINTER_FOR_REGISTERED_MEM,
      device));
  std::string name = "cuMemAllocHost (hostpointer)";
  if (!property) {
    std::cout << ">> " << name << ": skipped" << std::endl;
  } else {
    benchmark_allochost_hostpointer(
        "cuMemAllocHost (hostpointer)", n, v_input_1, v_input_2, v_output,
        v_output_reference, stream, device, num_repeats, num_warmups);
  }

  benchmark_hostalloc("cuMemHostAlloc", n, v_input_1, v_input_2, v_output,
                      v_output_reference, stream, device, flags, num_repeats,
                      num_warmups);
  flags = CU_MEMHOSTALLOC_DEVICEMAP;
  benchmark_hostalloc("cuMemHostAlloc (devicemap)", n, v_input_1, v_input_2,
                      v_output, v_output_reference, stream, device, flags,
                      num_repeats, num_warmups);
  flags = CU_MEMHOSTALLOC_WRITECOMBINED;
  benchmark_hostalloc("cuMemHostAlloc (writecombined)", n, v_input_1, v_input_2,
                      v_output, v_output_reference, stream, device, flags,
                      num_repeats, num_warmups);

  CHECK_CUDA_ERROR(cuDeviceGetAttribute(
      &property, CU_DEVICE_ATTRIBUTE_MANAGED_MEMORY, device));
  if (!property) {
    std::cout << ">> " << name << ": skipped" << std::endl;
  } else {
    for (int i = 0; i < 2; i++) {
      for (int j = 0; j < 2; j++) {
        const bool prefetch = i;
        name = "cuMemAllocManaged";

        const bool attach_host = j;

        if (prefetch) {
          name += "+cuMemPrefetchAsync";
        }

        name += attach_host ? " (host)" : " (global)";

        if (prefetch || attach_host) {
          CHECK_CUDA_ERROR(cuDeviceGetAttribute(
              &property, CU_DEVICE_ATTRIBUTE_CONCURRENT_MANAGED_ACCESS,
              device));
          if (!property) {
            std::cout << ">> " << name << ": skipped" << std::endl;
            continue;
          }
        }

        flags = attach_host ? CU_MEM_ATTACH_HOST : CU_MEM_ATTACH_GLOBAL;

        benchmark_allocmanaged(name.c_str(), n, v_input_1, v_input_2, v_output,
                               v_output_reference, stream, device, num_repeats,
                               prefetch ? 0 : num_warmups, flags, prefetch);
      }
    }

    if (property) {
      benchmark_hybrid("hybrid", n, v_input_1, v_input_2, v_output,
                       v_output_reference, stream, device, num_repeats,
                       num_warmups);
    }
  }

  CHECK_CUDA_ERROR(cuStreamDestroy(stream));
  CHECK_CUDA_ERROR(cuCtxDestroy(context));
}
