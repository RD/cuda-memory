ARCH=$(shell uname -m)

default: main-${ARCH}.x

main-%.x: main.cu
	nvcc $^ -o $@ -lcuda

